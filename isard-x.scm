(use-modules (gnu)
	     (gnu services desktop)
	     (gnu services docker)
	     (gnu services xorg)
	     (gnu services sysctl)
	     (gnu services ssh)
	     (gnu services databases)
	     (gnu services dbus)
	     (gnu services nix)
	     (gnu services virtualization)
	     (gnu packages)
	     (gnu packages golang)
	     (gnu packages databases)
	     (gnu packages spice)

	     ;; Build packages
	     (guix utils)
	     (guix build-system go)
	     (guix download)
	     (guix git-download)
	     (guix packages)
	     (guix gexp)
	     (guix records)
	     (gnu services)
	     (gnu services shepherd)
	     ((guix licenses) #:prefix license:))

(define-record-type* <spice-vdagent-configuration>
  spice-vdagent-configuration make-spice-vdagent-configuration
  spice-vdagent-configuration?
  (spice-vdagent spice-vdagent-configuration-spice-vdagent
                 (default spice-vdagent)))

(define (spice-vdagent-activation config)
  "Return the activation gexp for CONFIG."
  #~(begin
      (use-modules (guix build utils))
      (mkdir-p "/run/spice-vdagentd")))

(define (spice-vdagent-shepherd-service config)
  "Return a <shepherd-service> for spice-vdagentd with CONFIG."
  (define spice-vdagent (spice-vdagent-configuration-spice-vdagent config))

  (define spice-vdagentd-command
    (list
      (file-append spice-vdagent "/sbin/spice-vdagentd")
      "-x"))

  (list
    (shepherd-service
      (documentation "Spice vdagentd service")
      (requirement '(udev))
      (provision '(spice-vdagentd))
      (start #~(make-forkexec-constructor '#$spice-vdagentd-command))
      (stop #~(make-kill-destructor)))))

(define spice-vdagent-profile
  (compose list spice-vdagent-configuration-spice-vdagent))

(define spice-vdagent-service-type
  (service-type (name 'spice-vdagent)
    (extensions
      (list (service-extension shepherd-root-service-type
                               spice-vdagent-shepherd-service)
            (service-extension activation-service-type
                               spice-vdagent-activation)
            (service-extension profile-service-type
                               spice-vdagent-profile)))))

(define* (spice-vdagent-service
          #:optional (config (spice-vdagent-configuration)))
  "Start the @command{vdagentd} and @command{vdagent} daemons
from @var{spice-vdagent} to enable guest window resizing and
clipboard sharing."
  (service spice-vdagent-service-type config))

(define (go-google-golang-org-protobuf-package suffix)
  (package
    (name (string-append
	    "go-google-golang-org-protobuf-"
	    (string-replace-substring suffix "/" "-")))
    (version "1.25.0")
    (source
      (origin
	(method git-fetch)
	(uri (git-reference
	       (url "https://github.com/protocolbuffers/protobuf-go")
	       (commit (string-append "v" version))))
	(sha256
	  (base32 "0apfl42x166dh96zfq5kvv4b4ax9xljik6bq1mnvn2240ir3mc23"))))
    (build-system go-build-system)
    (arguments
      `(#:import-path ,(string-append "google.golang.org/protobuf/" suffix)
	#:unpack-path "google.golang.org/protobuf"))
    (home-page "https://developers.google.com/protocol-buffers")
    (synopsis "Go support for Google's protocol buffers")
    (description "Go support for Protocol Buffers")
    (license license:expat)))

(define-public protoc-gen-go
  (package (inherit (go-google-golang-org-protobuf-package "cmd/protoc-gen-go"))
    (name "protoc-gen-go")))

(define-public go-google-golang-org-protobuf-compiler-protogen
  (package (inherit (go-google-golang-org-protobuf-package "compiler/protogen"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-types-descriptorpb
  (package (inherit (go-google-golang-org-protobuf-package "types/descriptorpb"))))

(define-public go-google-golang-org-protobuf-types-pluginpb
  (package (inherit (go-google-golang-org-protobuf-package "types/pluginpb"))))

(define-public go-google-golang-org-protobuf-encoding-prototext
  (package (inherit (go-google-golang-org-protobuf-package "encoding/prototext"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-encoding-protowire
  (package (inherit (go-google-golang-org-protobuf-package "encoding/protowire"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-reflect-protodesc
  (package (inherit (go-google-golang-org-protobuf-package "reflect/protodesc"))))

(define-public go-google-golang-org-protobuf-reflect-protoreflect
  (package (inherit (go-google-golang-org-protobuf-package "reflect/protoreflect"))))

(define-public go-google-golang-org-protobuf-reflect-protoregistry
  (package (inherit (go-google-golang-org-protobuf-package "reflect/protoregistry"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-runtime-protoiface
  (package (inherit (go-google-golang-org-protobuf-package "runtime/protoiface"))))

(define-public go-google-golang-org-protobuf-runtime-protoimpl
  (package (inherit (go-google-golang-org-protobuf-package "runtime/protoimpl"))))

(define-public go-google-golang-org-protobuf-proto
  (package (inherit (go-google-golang-org-protobuf-package "proto"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-internal-encoding-defval
  (package (inherit (go-google-golang-org-protobuf-package "internal/encoding/defval"))))

(define-public go-google-golang-org-protobuf-internal-encoding-messageset
  (package (inherit (go-google-golang-org-protobuf-package "internal/encoding/messageset"))))

(define-public go-google-golang-org-protobuf-internal-encoding-text
  (package (inherit (go-google-golang-org-protobuf-package "internal/encoding/text"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-internal-encoding-tag
  (package (inherit (go-google-golang-org-protobuf-package "internal/encoding/tag"))))

(define-public go-google-golang-org-protobuf-internal-detrand
  (package (inherit (go-google-golang-org-protobuf-package "internal/detrand"))))

(define-public go-google-golang-org-protobuf-internal-errors
  (package (inherit (go-google-golang-org-protobuf-package "internal/errors"))))

(define-public go-google-golang-org-protobuf-internal-fieldsort
  (package (inherit (go-google-golang-org-protobuf-package "internal/fieldsort"))))

(define-public go-google-golang-org-protobuf-internal-filedesc
  (package (inherit (go-google-golang-org-protobuf-package "internal/filedesc"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-internal-filetype
  (package (inherit (go-google-golang-org-protobuf-package "internal/filetype"))))

(define-public go-google-golang-org-protobuf-internal-flags
  (package (inherit (go-google-golang-org-protobuf-package "internal/flags"))))

(define-public go-google-golang-org-protobuf-internal-genid
  (package (inherit (go-google-golang-org-protobuf-package "internal/genid"))))

(define-public go-google-golang-org-protobuf-internal-descfmt
  (package (inherit (go-google-golang-org-protobuf-package "internal/descfmt"))))

(define-public go-google-golang-org-protobuf-internal-descopts
  (package (inherit (go-google-golang-org-protobuf-package "internal/descopts"))))

(define-public go-google-golang-org-protobuf-internal-impl
  (package (inherit (go-google-golang-org-protobuf-package "internal/impl"))
    (native-inputs
      `(("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))))

(define-public go-google-golang-org-protobuf-internal-mapsort
  (package (inherit (go-google-golang-org-protobuf-package "internal/mapsort"))))

(define-public go-google-golang-org-protobuf-internal-pragma
  (package (inherit (go-google-golang-org-protobuf-package "internal/pragma"))))

(define-public go-google-golang-org-protobuf-internal-set
  (package (inherit (go-google-golang-org-protobuf-package "internal/set"))))

(define-public go-google-golang-org-protobuf-internal-strs
  (package (inherit (go-google-golang-org-protobuf-package "internal/strs"))))

(define-public go-google-golang-org-protobuf-internal-version
  (package (inherit (go-google-golang-org-protobuf-package "internal/version"))))

(define-public protoc-gen-go-grpc
  (package
    (name "protoc-gen-go-grpc")
    (version "1.34.0")
    (source
      (origin
	(method git-fetch)
	(uri (git-reference
	       (url "https://github.com/grpc/grpc-go")
	       (commit (string-append "v" version))))
	(sha256
	  (base32 "0s8y19qvv0zfy2irq8vv12h65v7pw18k6yqfix1j2370yxmsajgm"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
	#:unpack-path "google.golang.org/grpc"))
    (native-inputs
      `(("go-google-golang-org-protobuf-compiler-protogen" ,go-google-golang-org-protobuf-compiler-protogen)
	("go-google-golang-org-protobuf-types-pluginpb" ,go-google-golang-org-protobuf-types-pluginpb)
	("go-google-golang-org-protobuf-types-descriptorpb" ,go-google-golang-org-protobuf-types-descriptorpb)
	("go-google-golang-org-protobuf-encoding-prototext" ,go-google-golang-org-protobuf-encoding-prototext)
	("go-google-golang-org-protobuf-encoding-protowire" ,go-google-golang-org-protobuf-encoding-protowire)
	("go-google-golang-org-protobuf-reflect-protodesc" ,go-google-golang-org-protobuf-reflect-protodesc)
	("go-google-golang-org-protobuf-reflect-protoreflect" ,go-google-golang-org-protobuf-reflect-protoreflect)
	("go-google-golang-org-protobuf-reflect-protoregistry" ,go-google-golang-org-protobuf-reflect-protoregistry)
	("go-google-golang-org-protobuf-runtime-protoiface" ,go-google-golang-org-protobuf-runtime-protoiface)
	("go-google-golang-org-protobuf-runtime-protoimpl" ,go-google-golang-org-protobuf-runtime-protoimpl)
	("go-google-golang-org-protobuf-proto" ,go-google-golang-org-protobuf-proto)
	("go-google-golang-org-protobuf-internal-encoding-defval" ,go-google-golang-org-protobuf-internal-encoding-defval)
	("go-google-golang-org-protobuf-internal-encoding-messageset" ,go-google-golang-org-protobuf-internal-encoding-messageset)
	("go-google-golang-org-protobuf-internal-encoding-text" ,go-google-golang-org-protobuf-internal-encoding-text)
	("go-google-golang-org-protobuf-internal-encoding-tag" ,go-google-golang-org-protobuf-internal-encoding-tag)
	("go-google-golang-org-protobuf-internal-detrand" ,go-google-golang-org-protobuf-internal-detrand)
	("go-google-golang-org-protobuf-internal-errors" ,go-google-golang-org-protobuf-internal-errors)
	("go-google-golang-org-protobuf-internal-fieldsort" ,go-google-golang-org-protobuf-internal-fieldsort)
	("go-google-golang-org-protobuf-internal-filedesc" ,go-google-golang-org-protobuf-internal-filedesc)
	("go-google-golang-org-protobuf-internal-filetype" ,go-google-golang-org-protobuf-internal-filetype)
	("go-google-golang-org-protobuf-internal-flags" ,go-google-golang-org-protobuf-internal-flags)
	("go-google-golang-org-protobuf-internal-genid" ,go-google-golang-org-protobuf-internal-genid)
	("go-google-golang-org-protobuf-internal-descfmt" ,go-google-golang-org-protobuf-internal-descfmt)
	("go-google-golang-org-protobuf-internal-descopts" ,go-google-golang-org-protobuf-internal-descopts)
	("go-google-golang-org-protobuf-internal-impl" ,go-google-golang-org-protobuf-internal-impl)
	("go-google-golang-org-protobuf-internal-mapsort" ,go-google-golang-org-protobuf-internal-mapsort)
	("go-google-golang-org-protobuf-internal-pragma" ,go-google-golang-org-protobuf-internal-pragma)
	("go-google-golang-org-protobuf-internal-set" ,go-google-golang-org-protobuf-internal-set)
	("go-google-golang-org-protobuf-internal-strs" ,go-google-golang-org-protobuf-internal-strs)
	("go-google-golang-org-protobuf-internal-version" ,go-google-golang-org-protobuf-internal-version)))
    (home-page "https://github.com/grpc/grpc-go")
    (synopsis "The Go language implementation of gRPC. HTTP/2 based RPC")
    (description "The Go implementation of gRPC: A high performance, open source, general RPC framework that puts mobile and HTTP/2 first")
    (license license:expat)))

(define-public protoc-gen-go-grpc-mock
  (package
    (name "protoc-gen-go-grpc-mock")
    (version "0.2.0")
    (source
      (origin
	(method git-fetch)
	(uri (git-reference
	       (url "https://github.com/nefixestrada/protoc-gen-go-grpc-mock")
	       (commit (string-append "v" version))))
	(sha256
	  (base32 "0dgsd9kplpqzdfd78gr0yssw0iriljb05rcwlkzdqpg44ksdxl4g"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "github.com/nefixestrada/protoc-gen-go-grpc-mock"))
    (native-inputs
      `(("go-google-golang-org-protobuf-compiler-protogen" ,go-google-golang-org-protobuf-compiler-protogen)
	("go-google-golang-org-protobuf-types-pluginpb" ,go-google-golang-org-protobuf-types-pluginpb)
	("go-google-golang-org-protobuf-types-descriptorpb" ,go-google-golang-org-protobuf-types-descriptorpb)
	("go-google-golang-org-protobuf-encoding-prototext" ,go-google-golang-org-protobuf-encoding-prototext)
	("go-google-golang-org-protobuf-encoding-protowire" ,go-google-golang-org-protobuf-encoding-protowire)
	("go-google-golang-org-protobuf-reflect-protodesc" ,go-google-golang-org-protobuf-reflect-protodesc)
	("go-google-golang-org-protobuf-reflect-protoreflect" ,go-google-golang-org-protobuf-reflect-protoreflect)
	("go-google-golang-org-protobuf-reflect-protoregistry" ,go-google-golang-org-protobuf-reflect-protoregistry)
	("go-google-golang-org-protobuf-runtime-protoiface" ,go-google-golang-org-protobuf-runtime-protoiface)
	("go-google-golang-org-protobuf-runtime-protoimpl" ,go-google-golang-org-protobuf-runtime-protoimpl)
	("go-google-golang-org-protobuf-proto" ,go-google-golang-org-protobuf-proto)
	("go-google-golang-org-protobuf-internal-encoding-defval" ,go-google-golang-org-protobuf-internal-encoding-defval)
	("go-google-golang-org-protobuf-internal-encoding-messageset" ,go-google-golang-org-protobuf-internal-encoding-messageset)
	("go-google-golang-org-protobuf-internal-encoding-text" ,go-google-golang-org-protobuf-internal-encoding-text)
	("go-google-golang-org-protobuf-internal-encoding-tag" ,go-google-golang-org-protobuf-internal-encoding-tag)
	("go-google-golang-org-protobuf-internal-detrand" ,go-google-golang-org-protobuf-internal-detrand)
	("go-google-golang-org-protobuf-internal-errors" ,go-google-golang-org-protobuf-internal-errors)
	("go-google-golang-org-protobuf-internal-fieldsort" ,go-google-golang-org-protobuf-internal-fieldsort)
	("go-google-golang-org-protobuf-internal-filedesc" ,go-google-golang-org-protobuf-internal-filedesc)
	("go-google-golang-org-protobuf-internal-filetype" ,go-google-golang-org-protobuf-internal-filetype)
	("go-google-golang-org-protobuf-internal-flags" ,go-google-golang-org-protobuf-internal-flags)
	("go-google-golang-org-protobuf-internal-genid" ,go-google-golang-org-protobuf-internal-genid)
	("go-google-golang-org-protobuf-internal-descfmt" ,go-google-golang-org-protobuf-internal-descfmt)
	("go-google-golang-org-protobuf-internal-descopts" ,go-google-golang-org-protobuf-internal-descopts)
	("go-google-golang-org-protobuf-internal-impl" ,go-google-golang-org-protobuf-internal-impl)
	("go-google-golang-org-protobuf-internal-mapsort" ,go-google-golang-org-protobuf-internal-mapsort)
	("go-google-golang-org-protobuf-internal-pragma" ,go-google-golang-org-protobuf-internal-pragma)
	("go-google-golang-org-protobuf-internal-set" ,go-google-golang-org-protobuf-internal-set)
	("go-google-golang-org-protobuf-internal-strs" ,go-google-golang-org-protobuf-internal-strs)
	("go-google-golang-org-protobuf-internal-version" ,go-google-golang-org-protobuf-internal-version)))
    (home-page "https://github.com/nefixestrada/protoc-gen-go-grpc-mock")
    (synopsis "protoc-gen-go-grpc-mock is a protobuf plugin that autogenerates gRPC mocks in Go")
    (description "protoc-gen-go-grpc-mock is a protobuf plugin that autogenerates gRPC services mocks in Go using the github.com/stretchr/testify/mock package")
    (license license:expat)))

(operating-system
  (host-name "isard-dev")
  (timezone "Europe/Madrid")
  (bootloader (bootloader-configuration
		(bootloader grub-efi-bootloader)
		(target "/boot/efi")))
  (keyboard-layout (keyboard-layout "es" "cat"))
  (swap-devices
    (list "/swapfile"))
  (file-systems (append (list (file-system
				(device (file-system-label "root"))
				(mount-point "/")
				(type "ext4")))
			%base-file-systems))
  (users (append (list (user-account
			 (name "dev")
			 (group "users")
			 (home-directory "/home/dev")
			 (password (crypt "dev" "$6$abc"))
			 (supplementary-groups
			   '("audio"
			     "netdev"
			     "video"
			     "libvirt"
			     "docker"))))
		 %base-user-accounts))
  (sudoers-file
    (plain-file "sudoers"
		(string-append (plain-file-content %sudoers-specification)
			       (format #f "~a ALL = NOPASSWD: ALL ~%"
				       "dev"))))
  (services (append (list (service gnome-desktop-service-type)
			  (set-xorg-configuration
			    (xorg-configuration
			      (keyboard-layout keyboard-layout)))
			  (service sysctl-service-type
				   (sysctl-configuration
				     (settings '(("fs.inotify.max_user_watches" . "524288")))))
			  (spice-vdagent-service)
			  (service libvirt-service-type
				   (libvirt-configuration
				     (unix-sock-group "libvirt")))
			  (service docker-service-type)
			  (service nix-service-type)
			  (service postgresql-service-type
        (postgresql-configuration
          (postgresql postgresql-11)
            (config-file
            (postgresql-config-file
              (hba-file
                (plain-file "pg_hba.conf" "
                local	all	all			trust
                host    all             all             0.0.0.0/0            md5
                "))
              (extra-config
                '(("listen_addresses" "*")))))))
			  (service redis-service-type
          (redis-configuration
            (bind "0.0.0.0")))
        (service openssh-service-type))
		    %desktop-services))
  (packages (append 
	      (map specification->package
			 '(;; System packages
			   "nss-certs"
			   "gvfs"
			   "nix"
			   "fontconfig"
			   "font-google-noto"
			   "font-iosevka"
			   "ungoogled-chromium"

			   ;; General development packages
			   "go@1.14"
			   "make"
			   "gcc-toolchain"
			   "git"
			   "git-flow"
			   "neovim"
			   "tmux"
			   "redis"
			   "curl"
			   "graphviz"
			   "node@10.22"
			   "docker"
			   "docker-compose"

			   ;; Used for compiling the gRPC transport
			   "protobuf@3.14"
			   ;; Used for the hyper microservice
			   "pkg-config"
			   "libvirt"))

		    (list protoc-gen-go
			  protoc-gen-go-grpc
			  protoc-gen-go-grpc-mock)
		    %base-packages))
  (skeletons
    `((".bash_profile" ,(plain-file "bash_profile" "\
 # Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi\n"))
      (".bashrc" ,(plain-file "bashrc" "\
  # Bash initialization for interactive non-login shells and
  # for remote shells (info \"(bash) Bash Startup Files\").

  # Export 'SHELL' to child processes.  Programs such as 'screen'
  # honor it and otherwise use /bin/sh.
  export SHELL

  if [[ $- != *i* ]]
  then
      # We are being invoked from a non-interactive shell.  If this
      # is an SSH session (as in \"ssh host command\"), source
      # /etc/profile so we get PATH and other essential variables.
      [[ -n \"$SSH_CLIENT\" ]] && source /etc/profile

      # Don't do anything else.
      return
  fi

  # Source the system-wide file.
  source /etc/bashrc

  # Adjust the prompt depending on whether we're in 'guix environment'.
  if [ -n \"$GUIX_ENVIRONMENT\" ]
  then
      PS1='\\u@\\h \\w [env]\\$ '
  else
      PS1='\\e[1;36m\\u@\\h \\w\\$ \\e[m'
  fi
  alias ls='ls -p --color=auto'
  alias ll='ls -l'
  alias grep='grep --color=auto'
  alias vim='nvim'

  source /run/current-system/profile/etc/profile.d/nix.sh
  export PATH=$PATH:~/.nix-profile/bin:~/go/bin

  #
  # Isard Development
  #

  if [ ! -f ~/.config/.isard-dev-configured ]; then
    sudo -u root sudo -u postgres createuser -h /var/run/postgresql -ds dev
    sudo -u root sudo -u postgres psql -h /var/run/postgresql -c \"ALTER USER dev WITH PASSWORD 'dev';\"
    createdb -h /var/run/postgresql isard

    nix-channel --add https://nixos.org/channels/nixos-unstable nixos
    nix-channel --update

    ln -s /nix/var/nix/profiles/per-user/$(whoami)/profile ~/.nix-profile

    nix-env -iA nixos.vscodium

    mkdir -p ~/.config/VSCodium/User
    if [ ! -f ~/.config/VSCodium/User/settings.json  ]; then
        echo '{
            \"workbench.iconTheme\": \"material-icon-theme\",
            \"workbench.colorTheme\": \"Ayu Mirage Bordered\",
            \"go.useLanguageServer\": true,
            \"[go]\": {
                \"editor.formatOnSave\": true,
                \"editor.codeActionsOnSave\": {
                    \"source.organizeImports\": true,
                },
            },
            \"[go.mod]\": {
                \"editor.formatOnSave\": true,
                \"editor.codeActionsOnSave\": {
                    \"source.organizeImports\": true,
                },
            },
            \"gopls\": {
                \"usePlaceholders\": true,
                \"staticcheck\": false,
            }
        }'> ~/.config/VSCodium/User/settings.json
    fi

    codium --install-extension golang.go
    codium --install-extension pkief.material-icon-theme
    codium --install-extension zxh404.vscode-proto3
    codium --install-extension redhat.vscode-yaml
    codium --install-extension teabyii.ayu
    codium --install-extension hediet.vscode-drawio
    codium --install-extension octref.vetur
    codium --install-extension eamodio.gitlens
    codium --install-extension graphql.vscode-graphql
    codium --install-extension esbenp.prettier-vscode

    mkdir -p ~/.config/chromium
    echo '{\"browser\":{\"enabled_labs_experiments\":[\"extension-mime-request-handling@2\"]}}' > ~/.config/chromium/Local\\ State
    # To add a new plugin: https://clients2.google.com/service/update2/crx?response=redirect&prodversion=87.0&x=id%3D~~~PLUGIN ID~~~%26installsource%3Dondemand%26uc&acceptformat=crx3
    chromium \
    	\"https://clients2.google.com/service/update2/crx?response=redirect&prodversion=87.0&x=id%3Dnhdogjmejiglipccpnnnanhbledajbpd%26installsource%3Dondemand%26uc&acceptformat=crx3\" \
	\"https://clients2.google.com/service/update2/crx?response=redirect&prodversion=87.0&x=id%3Dcjpalhdlnbpafiamejdnhcphjbkeiagm%26installsource%3Dondemand%26uc&acceptformat=crx3\"
    
    go get -v golang.org/x/tools/gopls
    go get -v golang.org/x/tools/cmd/stringer

    mkdir -p ~/.local/share
    ln -s /run/current-system/profile/share/fonts ~/.local/share/fonts

    sudo mkdir -p /data/dev
    sudo chown -R $(whoami):users /data/dev

    if [ ! -f /data/dev/.env ]; then
      echo '# For example, nefix' >> /data/dev/.env
      echo 'GITLAB_USER=\"\"' >> /data/dev/.env
      echo '# For example, Néfix Estrada' >> /data/dev/.env
      echo 'GITLAB_NAME=\"\"' >> /data/dev/.env
      echo '# For example, nefixestrada@gmail.com' >> /data/dev/.env
      echo 'GITLAB_EMAIL=\"\"' >> /data/dev/.env
      nvim /data/dev/.env
    fi

    [ -f /data/dev/.env ] && export $(grep -v '^#' /data/dev/.env | xargs)

    echo \"[user]
    name = ${GITLAB_NAME}
    email = ${GITLAB_EMAIL}\" > ${HOME}/.gitconfig

    mkdir /data/dev/workspace
    cd /data/dev/workspace

    for repo in isardvdi; do
          if [ ! -d ${repo}/.git ]; then
              git clone https://gitlab.com/${GITLAB_USER}/${repo}
              cd ${repo}

              git config url.\"https://${GITLAB_USER}@gitlab.com\".InsteadOf \"https://gitlab.com\"
              git remote add upstream https://gitlab.com/isard/${repo}

              git flow init -d

              [ -f go.mod ] && go mod tidy

              cd -
          fi
    done

    touch ~/.config/.isard-dev-configured
  else
    cd /data/dev/workspace
  fi
  \n")))))
